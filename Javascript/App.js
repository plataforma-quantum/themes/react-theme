import { InertiaApp } from "@inertiajs/inertia-react";
import { render } from "react-dom";
import React from "react";

const app = document.getElementById("app");

render(
    <InertiaApp
        initialPage={JSON.parse(app.dataset.page)}
        resolveComponent={name => require(`./Pages/${name}`).default}
    />,
    app
);
