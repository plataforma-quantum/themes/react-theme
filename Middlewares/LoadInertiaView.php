<?php

namespace Themes\React\Middlewares;

use Closure;
use Inertia\Inertia;

class LoadInertiaView
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Inertia::setRootView("React::Master");
        return $next($request);
    }
}
