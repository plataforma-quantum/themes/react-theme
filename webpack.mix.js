const liveReloadPlugin = require("webpack-livereload-plugin");
const mix = require("laravel-mix");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application, as well as bundling up your JS files.
 |
 */

mix.webpackConfig({
    plugins: [new liveReloadPlugin()],
})
    .setPublicPath("Public/")
    .react("Javascript/App.js", "Js/")
    .sass("Sass/App.scss", "Css");
