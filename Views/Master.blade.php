<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>React - Welcome to the theme</title>

    <link rel="stylesheet" href="{{url('assets/themes/react/Css/App.css')}}">

</head>

<body>
    @inertia

    <script src="{{url('assets/themes/react/Js/App.js')}}"></script>
    @if(config('app.env') == 'local')
    <script src="http://localhost:35729/livereload.js"></script>
    @endif
</body>

</html>
