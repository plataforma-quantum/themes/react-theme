# Getting started

Install theme

```bash
php artisan quantum:theme-install react
```

Install npm dependencies

```bash
cd themes/React
npm install
```

Start the development server

```bash
cd themes/React
npm run watch
```
