<?php

use Themes\React\Middlewares\LoadInertiaView;
use Illuminate\Support\Facades\Route;
use Illuminate\Routing\Router;

$config = [
    'middleware' => ['web', LoadInertiaView::class],
    'namespace'  => 'Themes\\React\\Controllers'
];

Route::group($config, function (Router $router) {
    $router->get("/", "HomeController@index");
});
